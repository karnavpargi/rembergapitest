const { Assets, Selections } = require('../../models');
const _ = require('lodash');
module.exports = {
  Get: async (req, res) => {
    try {
      let { pageIndex: page = 1, pageSize: limit = 99999, search } = req.query;
      page = parseInt(page);
      limit = parseInt(limit);
      const skip = (page - 1) * limit;
      let filter = {};
      if (!_.isEmpty(search)) {
        let Reg = { $regex: new RegExp(search, 'i') };
        filter = {
          $or: [{ name: Reg }, { owner: Reg }, { company_name: Reg }],
        };
      }

      const assets = Assets.find(filter).skip(skip).limit(limit);
      const counts = Assets.countDocuments(filter);

      const [data, total] = await Promise.all([assets, counts]);
      return res.send({ error: false, total, data });
    } catch (error) {
      return res.send({
        data: [],
        error: true,
        message: error.message,
      });
    }
  },
  Update: async (req, res) => {
    const { ids = [], search = null, isAllSelected = false } = req.body;
    let selection = ids;
    let filter = {};
    if (isAllSelected) {
      if (!_.isEmpty(search)) {
        let Reg = { $regex: new RegExp(search, 'i') };
        filter = {
          $or: [{ name: Reg }, { owner: Reg }, { company_name: Reg }],
        };
      }
      let data = await Assets.find(filter).distinct('_id');
      if (data.length) selection = data;
    }

    await Selections.deleteMany();
    Selections.insertMany({ selection }).then((data) => {
      return res.send({ total: data.length && data[0].selection ? data[0].selection.length : 0, data });
    });
  },
  GetSelections: (req, res) => {
    var page = parseInt(req.query.pageIndex) || 1;
    var limit = parseInt(req.query.pageSize) || 9999;
    const skip = (page - 1) * limit;
    Selections.find()
      .skip(skip)
      .limit(limit)
      .then((data) => {
        return res.send({ data });
      });
  },
};

// setTimeout(async () => {
//   let data = await Assets.find().distinct('_id');
//   console.log(data);
// }, 2000);
