module.exports = {
  configs: require('./configs'),
  mongoose: require('./connections'),
};
